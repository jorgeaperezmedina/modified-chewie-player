// import 'package:chewie_prep/chewie_list_item.dart';
// import 'package:news_app/widgets/ui_elements/video_player/chewie_list_item.dart';
// import './chewie_list_item.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/services.dart';
import 'package:chewie/chewie.dart';
import 'package:screen/screen.dart';
// import 'package:news_app/libraries.dart';
import 'package:auto_orientation/auto_orientation.dart';
class Video extends StatefulWidget {
  final String urlVideo;
  // final VideoPlayerController videoPlayerController;
  Video(this.urlVideo);
  @override
  State<StatefulWidget> createState() {
    return _VideoPlayerState();
  }
}

class _VideoPlayerState extends State<Video>{
  // String urlVideo;
  ChewieController _chewieController;
  VideoPlayerController videoPlayerController;
  @override
  void initState() {
    super.initState();
    Screen.keepOn(true);
    // setLandscapeOrientation();
    AutoOrientation.landscapeMode();
    videoPlayerController = VideoPlayerController.network(widget.urlVideo);
    _chewieController = ChewieController(
        videoPlayerController: videoPlayerController,
        autoPlay: true,
        allowFullScreen: Platform.isAndroid ? true : false,
        // fullScreenByDefault: true,
        //closeButton: Platform.isIOS ? true : false,
        // deviceOrientationsAfterFullScreen: [
        //   DeviceOrientation.landscapeLeft,
        //   DeviceOrientation.landscapeRight
        // ],
      errorBuilder: (context, errorMessage) {
        print("Ocurrio un error con el video, ocurrio un error adair");
        print(errorMessage);
        // _chewieController.dispose();
        // videoPlayerController.dispose();
        // dispose();
        Navigator.pop(context, false);
        Future.value(false);
        // super.dispose();
        return Text("");
        // return Center(
        //   child: Text(errorMessage, style: TextStyle(color: Colors.white)),
        // );
        // return Scaffold(
        //   body: Text(errorMessage, style: TextStyle(color: Colors.yellow, fontSize: 34.0)),
        // );

        // await dispose();
        // return Future.value(false);
        // return Scaffold.of(context).showSnackBar(new SnackBar(
        //   content: new Text("Sending Message"),
        // ));
      }  
    );
  }

  @override
  Widget build(BuildContext context) {
    // setLandscapeOrientation();
    if (widget.urlVideo.length == 0) {
      return WillPopScope(
          onWillPop: () {
            // setPortraitOrientation();
            // Navigator.pop(context); // this line i add beacause not close the menu_letf, this work but i am not sure this is the correct asnwer.
            Navigator.pop(context, false);
            return Future.value(false);
          },
          child: Container(
            child: Text("Sin url del video"),
          ));
    } else {
       // _chewieController.enterFullScreen();
      // _chewieController.toggleFullScreen();
      return WillPopScope(
        
        onWillPop: () {
          // setPortraitOrientation();
          // Navigator.pop(context); // this line i add beacause not close the menu_letf, this work but i am not sure this is the correct asnwer.
          Navigator.pop(context, false);
          return Future.value(false);
          
        },
        child: Container(
          // padding: EdgeInsets.all(8.0), 
          child: Chewie(
            
            controller: _chewieController,
          ),
        // child: Container(
        //   child: ChewieListItem(
        //     videoPlayerController: VideoPlayerController.network(urlVideo),
        //     // VideoPlayerController.network('http://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4')
        //     // ..initialize().then((_) {
        //     //   // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        //     //   setState(() {});
        //     // });
        //   ),
        )
        );
    }
  }

  @override
  void dispose() async {
    Screen.keepOn(false);
    videoPlayerController.dispose();
    _chewieController.dispose();
    // setPortraitOrientation();
    AutoOrientation.portraitMode();
    super.dispose();
  }
}