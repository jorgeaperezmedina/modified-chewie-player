import 'dart:io';
import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:chewie/src/chewie_player.dart';
import 'package:chewie/src/chewie_progress_colors.dart';
import 'package:chewie/src/material_progress_bar.dart';
import 'package:chewie/src/utils.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class MaterialControls extends StatefulWidget {
  const MaterialControls({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MaterialControlsState();
  }
}

class _MaterialControlsState extends State<MaterialControls> {
  VideoPlayerValue _latestValue;
  double _latestVolume;
  bool _hideStuff = true;
  Timer _hideTimer;
  Timer _initTimer;
  Timer _showAfterExpandCollapseTimer;
  bool _dragging = false;
  bool _displayTapped = false;

  final barHeight = 40.0;
  final marginSize = 5.0;

  VideoPlayerController controller;
  ChewieController chewieController;
  // Map<dynamic, dynamic> _source = {ConnectivityResult.none: false};
  // MyConnectivity _connectivity = MyConnectivity._instance;



  @override
  Widget build(BuildContext context) {

// print(_latestValue.hasError);
    // // print(chewieController.connection);
    // // print(chewieController.connection.keys.toList()[0]);
    // // print(chewieController.connection.keys.toList()[0]);
    // // if (chewieController.connection == false) {
    // //   print('llegue al if antes de entrar al chewie');
    // //   print(chewieController.connection);
    // //    controller.pause(); 
    // //   controller.play();

    // // }
    // // print(_latestValue.hasError);
    // if (_latestValue.hasError) {
    //   // print(_latestValue.hasError);
    //   // controller.dispose();
    //   // chewieController.dispose();
    //   // chewieController.exitFullScreen();
    //   // chewieController.routePageBuilder;
    //   return chewieController.errorBuilder != null
    //       ? chewieController.errorBuilder(
    //           context,
    //           chewieController.videoPlayerController.value.errorDescription,
    //         )
    //         : Center(
    //   child: Container(
    //     decoration: BoxDecoration(
    //         color: Colors.black, borderRadius: BorderRadius.circular(40.0)),
    //     child: IconButton(
    //       icon: Icon(Icons.refresh),
    //       iconSize: 40.0,
    //       color: Colors.white,
    //       onPressed: () {
    //         changeStateVideo();
    //       },
    //     ),
    //   ),
    // );
    //       //     child: CircularProgressIndicator()
    //       //   );
    // }

    


    return MouseRegion(
      onHover: (_) {
        _cancelAndRestartTimer();
      },
      child: Container(
        decoration: BoxDecoration(
          color: _hideStuff ? Colors.transparent : Colors.black.withOpacity(0.5)
        ),
        child: GestureDetector(
        onTap: () => _cancelAndRestartTimer(),
        child: AbsorbPointer(
          absorbing: _hideStuff,
          child: Column(
            children: <Widget>[
            //  titleVideo(context),
            Container(
              margin: EdgeInsets.only(top: 10.0 , left: 10),
              child: rowtitle(),
            ),
            // _buildButtonPausePlay(),
            // _buildButtonPausePlay(),
             
              _buildhitArea(),
              _buildBottomBar(context)
              // _latestValue != null &&
              //             !_latestValue.isPlaying &&
              //             _latestValue.duration == null ||
              //         _latestValue.isBuffering
              //         // ?  Center(
              //         //   child: exitFullScreen(),
              //         // )
              //     ? 
              //     const Expanded(
              //         child: const Center(
              //           child: const CircularProgressIndicator(),
              //         ),
              //       )
              //       // : topTitle(),
              //     : _buildHitArea(),
                
              //       titleVideo(context),
                  
              // _buildBottomBar(context),
            ],
          ),
        ),
      ),
      ),
    );
  }

  @override
  void dispose() {
    _dispose();
    // chewieController.dispose();
    // _connectivity.disposeStream();
    super.dispose();
  }

  void _dispose() {
    controller.removeListener(_updateState);
    _hideTimer?.cancel();
    _initTimer?.cancel();
    _showAfterExpandCollapseTimer?.cancel();
  }

  @override
  void didChangeDependencies() {
    final _oldController = chewieController;
    chewieController = ChewieController.of(context);
    controller = chewieController.videoPlayerController;

    if (_oldController != chewieController) {
      _dispose();
      _initialize();
    }

    super.didChangeDependencies();
  }

  Widget _buildButtonPausePlay(){
    return _latestValue != null &&
                          !_latestValue.isPlaying &&
                          _latestValue.duration == null ||
                      _latestValue.isBuffering ?
                      _buildhitArea() : _buildButtonPausePlay();
  }

  Widget _buildhitArea(){
    return _latestValue != null &&
                          !_latestValue.isPlaying &&
                          _latestValue.duration == null ||
                      _latestValue.isBuffering ?
                      const Expanded(
                      child: const Center(
                        child: const CircularProgressIndicator(),
                      ),
                    )
                    // : topTitle(),
                  : _buildHitArea();
                  // : _buildPlayPause(controller);
  }

  Widget rowtitle(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        // Expanded(
        //   flex: 2,
        //   child: Container(),
        // ),
        Expanded(
          // flex: 5,
          child: Container(
            margin: EdgeInsets.only(left: 15, top: 12),
            child: titleVideo(context),
          ),
          // child: SingleChildScrollView(
          //   scrollDirection: Axis.horizontal,
          //   child: titleVideo(context),
          // ),
        ),
        // Expanded(
        //   flex: 2,
        //   child: Container(),
        // )
      ],
    );
  }
  
  AnimatedOpacity titleVideo(BuildContext context){
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 0.9 ,
      duration: Duration(milliseconds: 300),
      child: Align(
      alignment: Alignment.topLeft,
      child: Container(
        // margin: EdgeInsets.only(left: 10, right: 10, bottom: 2),
        decoration: BoxDecoration(
          // color: Colors.black45
        ),
         child: chewieController.isLive ? Text('') : Text( chewieController.titleVideo,
      //   child: chewieController.isLive ? Text('') : Text("Ep."+chewieController.numberEpisode.toString()+ 
      // " " + chewieController.titleVideo,
    style: TextStyle(
      // decoration: TextDecoration.underline,
      color: Colors.white,
      fontSize: 17,
      shadows: [
        Shadow(
          color: Colors.black,
          blurRadius: 10.0,
          offset: Offset(5.0, 5.0)
        ),

        Shadow(
          color: Colors.black,
          blurRadius: 10.0,
          offset: Offset(5.0, 5.0)
        )
      ]
      ,), maxLines: 1, overflow: TextOverflow.ellipsis
    ,),
      )
    ),
    );
  }

  

  AnimatedOpacity _buildBottomBar(
    BuildContext context,
  ) {
    final iconColor = Theme.of(context).textTheme.button.color;

    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 0.9,
      duration: Duration(milliseconds: 300),
      child: Container(
        margin: EdgeInsets.all(5),
        height: barHeight,
        
        decoration: BoxDecoration(
          // color: Colors.black87,
          //color: Color(0xFFedeae9),
          borderRadius: BorderRadius.circular(1.0),
          // boxShadow: [
          //     BoxShadow(color: Colors.black12, blurRadius: 2.0, offset: Offset(1.0, 1.0))
          // ]
        ),
        // color: Theme.of(context).dialogBackgroundColor,
        child: Row(
          children: <Widget>[
            // _buildPlayPause(controller),
            chewieController.isLive
                ? Expanded(child: const Text('', style: TextStyle(color: Colors.redAccent, fontSize: 17, fontWeight: FontWeight.bold),))
                : _buildPosition(iconColor),
            chewieController.isLive ? const SizedBox() : _buildProgressBar(),
            // chewieController.allowMuting
            //     ? _buildMuteButton(controller)
            //     : Container(),
                // chewieController.isLive ? Container() : _buildVideoQuality(),
            chewieController.allowFullScreen && !chewieController.isLive
                ? _buildExpandButton()
                : Container(),

                chewieController.allowFullScreen && chewieController.isLive ?
                 rowLiveStream() : Container(),
          ],
        ),
      ),
    );
  }

  Widget rowLiveStream(){
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(3),
          child: Icon(Icons.radio_button_checked, color: Colors.redAccent, size: 11,),
        ),
        Text('En vivo', style:  TextStyle(color: Colors.white, shadows: [
          Shadow(
            color: Colors.black,
            blurRadius: 10.0,
            offset: Offset(5.0, 5.0)
          )
        ], fontSize: 17),
        ),
        _buildExpandButton()
      ],
    );
  }
  

  GestureDetector _buildExpandButton() {
    return GestureDetector(
      onTap: _onExpandCollapse,
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: Container(
          height: barHeight,
          margin: EdgeInsets.only(right: 12.0),
          padding: EdgeInsets.only(
            left: 8.0,
            right: 8.0,
          ),
          child: Center(
            child: Icon(
              chewieController.isFullScreen
                  ? Icons.fullscreen_exit
                  : Icons.fullscreen,
                  color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Expanded _buildHitArea() {
    return Expanded(
      child: Container(
        child: GestureDetector(
        onTap: () {
          if (_latestValue != null && _latestValue.isPlaying) {
            if (_displayTapped) {
              setState(() {
                _hideStuff = true;
              });
            } else
              _cancelAndRestartTimer();
          } else {
            _playPause();

            setState(() {
              _hideStuff = true;
            });
          }
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
        //     Container(
        //   child: AnimatedOpacity(
        //       opacity:_displayTapped && _hideStuff == false  ? 1.0 : 0.0, 
        //       duration: Duration(milliseconds: 300),
        //       child: Icon(Icons.replay_10, color: Colors.white, size: 50,),
        //   ),
        // ),
          Container(
            child: AnimatedOpacity(
              opacity:_hideStuff == false  ? 1.0 : 0.0,
              duration: Duration(milliseconds: 300),
              child: _buildPlayPause(controller),
            ),
          
        ),
        // Container(
        //   child: AnimatedOpacity(
        //       opacity:_displayTapped && _hideStuff == false  ? 1.0 : 0.0, 
        //       duration: Duration(milliseconds: 300),
        //       child: Icon(Icons.forward_10, color: Colors.white, size: 50,),
        //   ),
        // )
        ],)
        // Container(
        //   color: Colors.transparent,
        //   child: Center(
        //     child: AnimatedOpacity(
        //       opacity:
        //           _latestValue != null && !_latestValue.isPlaying && !_dragging
        //               ? 1.0
        //               : 0.0,
        //       duration: Duration(milliseconds: 300),
        //       child: GestureDetector(
        //         child: Container(
        //           decoration: BoxDecoration(
                    
        //             color: Colors.black87,
        //             // color: Theme.of(context).dialogBackgroundColor,
        //             borderRadius: BorderRadius.circular(48.0),
        //           ),
        //           child: 
        //             Padding(
        //             padding: EdgeInsets.all(12.0),
        //             child: Icon(Icons.play_arrow, size: 30.0, color: Colors.white, ),
        //           ),
        //         ),
        //       ),
        //     ),
        //   ),
        // ),
      ),
      )
    );
  }
  

  GestureDetector _buildMuteButton(
    VideoPlayerController controller,
  ) {
    return GestureDetector(
      onTap: () {
        _cancelAndRestartTimer();

        if (_latestValue.volume == 0) {
          controller.setVolume(_latestVolume ?? 0.5);
        } else {
          _latestVolume = controller.value.volume;
          controller.setVolume(0.0);
        }
      },
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRect(
          child: Container(
            child: Container(
              height: barHeight,
              padding: EdgeInsets.only(
                left: 8.0,
                right: 8.0,
              ),
              child: Icon(
                (_latestValue != null && _latestValue.volume > 0)
                    ? Icons.volume_up
                    : Icons.volume_off,
                    color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
  GestureDetector _buildVideoQuality(){
    return GestureDetector(

      child: Container(
        height: barHeight,
        color:  Colors.transparent,
        margin: EdgeInsets.only(left: 8.0, right: 4.0),
        padding: EdgeInsets.only(
          left: 12.0,
          right: 12.0,
        ),
        child: Icon(Icons.settings, color: Colors.white),
      ),
      onTap: (){
          _showBottomModal(context);
      },
    );
  }


  Future _showBottomModal(BuildContext context) async {
    // await generateOption(video);
    await showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext _context) {
          return Container(
            // color: ,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: _generateOptions(context, chewieController.videoQuality),
              ),
            ),
            decoration: BoxDecoration(
                color: Theme.of(_context).canvasColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
          );
        });
  }


  List<Widget> _generateOptions(BuildContext context, List<dynamic> videos) {
    List<Widget> listTiles =  List<Widget>();
    listTiles.add(Stack(
      children: <Widget>[
        Container(
          color: Colors.black,
          width: double.infinity,
          height: 56.0,
          child: Center(
            child: Text(
              "Calidad",
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        )
      ],
    ));
    if (videos.length == 0 || videos.length == null) {
       listTiles.add(ListTile(
         title: Text('Sin calidad de video disponible', style: TextStyle(color: Colors.white),),
         onTap: (){
           Navigator.of(context).pop();
         },
      )); 
    }
    for (var i = 0; i < videos.length; i++) {
      listTiles.add(
        Container(
          color: Colors.black,
          child: ListTile(
        // leading: Icon(Icons.hd), + "(${videos[i].height.toString()})"
        title: Row(
          children: <Widget>[
             Text(videos[i].videoType.toString().toUpperCase() + " ", 
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold
        ),
        ),
         Text("(${videos[i].height.toString()})",
         style: TextStyle(color: Colors.white),
        )
          ],
        ),
        onTap: () {
            // chewieController.dispose();
            // controller.pause();
            setState(() {
              chewieController.dispose();
              controller.pause();
              // controller.dispose();
              // chewieController.seekTo(Duration.zero);
              // controller.durationChewie(Duration.zero);
                controller.seekTo(Duration.zero);
              
              chewieController = ChewieController(
                          videoPlayerController: controller = VideoPlayerController.network(videos[i].urlVideo.toString()).initialize() as VideoPlayerController,
                          aspectRatio: 16 / 9,
                          autoPlay: true,
                          isLive: chewieController.isLive,
                          durationSpecific: chewieController.durationSpecific,
                          allowFullScreen: Platform.isIOS ? true : false,
                          titleVideo: chewieController.titleVideo,
                          // titleSerie: '',
                          videoQuality: chewieController.videoQuality,
                          closeButton: Platform.isIOS ? true : false,
                          autoInitialize: true,
                          // showControlsOnInitialize: true
                    );
              // changeStateVideo(videos[i].urlVideo.toString());
            });
          // Navigator.of(context).dispose();
        //   Navigator.of(context).push<void>( PageRouteBuilder(
        // opaque: true,
        // transitionDuration: const Duration(milliseconds: 1000),
        // pageBuilder: (BuildContext context, _, __) {
        //   return VideoChangeQuality(
        //     url: videos[i].urlVideo.toString(),
        //     duration: chewieController.durationSpecific,
        //     isallowFullScreen: chewieController.allowFullScreen,
        //     islive: chewieController.isLive,
        //     numberEpisode: chewieController.numberEpisode,
        //     title: chewieController.titleVideo,
        //     quality: chewieController.videoQuality,
        //   );
        //   // return new VideoPlayer(url, duration);
        // }));
          // Navigator.of(context).pushReplacement( MaterialPageRoute<void>(builder: (BuildContext context) => VideoChangeQuality(
          //   url: videos[i].urlVideo.toString(),
          //   duration: chewieController.durationSpecific,
          //   isallowFullScreen: chewieController.allowFullScreen,
          //   islive: chewieController.isLive,
          //   numberEpisode: chewieController.numberEpisode,
          //   title: chewieController.titleVideo,
          //   quality: chewieController.videoQuality,
          // )));
              // chewieController.dispose();
              // controller =VideoPlayerController.network(videos[i].urlVideo.toString());
              // setState(() {
              //   chewieController.pause();
              //   // controller.dispose();
              //   controller.pause();
              //   // chewieController.pause();
              //   controller.seekTo(Duration.zero);
              //   chewieController.seekTo(Duration.zero);
              //   // chewieController.seekTo(chewieController.durationSpecific);
              //       chewieController.videoPlayerController.durationChewie(chewieController.durationSpecific);
              //   chewieController.dispose();
              //   // chewieController.videoPlayerController.durationChewie(chewieController.durationSpecific);
              //       chewieController = ChewieController(
              //             videoPlayerController: controller,
              //             aspectRatio: 16 / 9,
              //             autoPlay: true,
              //             isLive: chewieController.isLive,
              //             durationSpecific: chewieController.durationSpecific,
              //             allowFullScreen: Platform.isIOS ? true : false,
              //             titleVideo: chewieController.titleVideo,
              //             titleSerie: '',
              //             videoQuality: chewieController.videoQuality,
              //             numberEpisode: chewieController.numberEpisode,
              //             closeButton: Platform.isIOS ? true : false,
              //       );
              //       // chewieController.play();
              //   // controller = VideoPlayerController.network(videos[i].urlVideo.toString());
              //   // controller.play();
              //   _initialize();
              // });
          // Navigator.of(context).pop();
          // _playVideo(context, videos[i].urlVideo, episode.media.videos);
        },
      ),
        )
        );
    }
    return listTiles;
  }

  
  
  void changeStateVideo(String url){
    setState(() {
                chewieController.dispose();
                // controller.dispose();
                controller.pause();
                // chewieController.pause();
                controller.seekTo(Duration.zero);
                chewieController.seekTo(Duration.zero);
                // chewieController.seekTo(chewieController.durationSpecific);
                    chewieController.videoPlayerController.durationChewie(chewieController.durationSpecific);
                
                // _initialize();
                // chewieController.videoPlayerController.durationChewie(chewieController.durationSpecific);
                    chewieController = ChewieController(
                          videoPlayerController: controller = VideoPlayerController.network(url),
                          aspectRatio: 16 / 9,
                          autoPlay: true,
                          isLive: chewieController.isLive,
                          durationSpecific: chewieController.durationSpecific,
                          allowFullScreen: Platform.isIOS ? true : false,
                          titleVideo: chewieController.titleVideo,
                          // titleSerie: '',
                          videoQuality: chewieController.videoQuality,
                          closeButton: Platform.isIOS ? true : false,
                          autoInitialize: true,
                          showControlsOnInitialize: true
                    );
                    // chewieController.play();
                // controller = VideoPlayerController.network(videos[i].urlVideo.toString());
                // controller.play();
              });
  }

  GestureDetector _buildPlayPause(VideoPlayerController controller) {
    return GestureDetector(
      onTap: _playPause,
      child: Container(
        height: barHeight,
        color: Colors.transparent,
        margin: EdgeInsets.only(left: 8.0, right: 4.0),
        padding: EdgeInsets.only(
          left: 12.0,
          right: 12.0,
        ),
        child: Icon(
          controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
          color: Colors.white,
          size: chewieController.isLive && !chewieController.isFullScreen  ? 30 : 50,
        ),
      ),
    );
  }

  Widget _buildPosition(Color iconColor) {
    final position = _latestValue != null && _latestValue.position != null
        ? _latestValue.position
        : Duration.zero;
    final duration = _latestValue != null && _latestValue.duration != null
        ? _latestValue.duration
        : Duration.zero;

    return Padding(
      padding: EdgeInsets.only(right: 24.0),
      child: Text(
        '${formatDuration(position)} / ${formatDuration(duration)}',
        style: TextStyle(
          color: Colors.white,
          fontSize: 15.0,
        ),
      ),
    );
  }

  void _cancelAndRestartTimer() {
    _hideTimer?.cancel();
    _startHideTimer();

    setState(() {
      _hideStuff = false;
      _displayTapped = true;
    });
  }

  Future<Null> _initialize() async {
    controller.addListener(_updateState);

    _updateState();

    if ((controller.value != null && controller.value.isPlaying) ||
        chewieController.autoPlay) {
      _startHideTimer();
    }

    if (chewieController.showControlsOnInitialize) {
      _initTimer = Timer(Duration(milliseconds: 200), () {
        setState(() {
          _hideStuff = false;
        });
      });
    }
  }

  void _onExpandCollapse() {
    setState(() {
      _hideStuff = true;

      chewieController.toggleFullScreen();
      _showAfterExpandCollapseTimer = Timer(Duration(milliseconds: 300), () {
        setState(() {
          _cancelAndRestartTimer();
        });
      });
    });
  }

  void _playPause() {
    bool isFinished = _latestValue.position >= _latestValue.duration;

    setState(() {
      if (controller.value.isPlaying ) {
        print(controller.value.isPlaying);
        _hideStuff = false;
        _hideTimer?.cancel();
        controller.pause();
      } else {
        _cancelAndRestartTimer();

        if (!controller.value.initialized ) {
          print(controller.value.isPlaying);
          controller.initialize().then((_) {
            controller.play();
          });
        } else {
          if (isFinished) {
            controller.seekTo(Duration(seconds: 0));
          }
          controller.play();
        }
      }
    });
  }

  void _startHideTimer() {
    _hideTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  void _updateState() {
    setState(() {
      _latestValue = controller.value;
    });
  }

  Widget _buildProgressBar() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(right: 20.0),
        child: MaterialVideoProgressBar(
          controller,
          cheWieController: chewieController,
          onDragStart: () {
            setState(() {
              _dragging = true;
            });

            _hideTimer?.cancel();
          },
          onDragEnd: () {
            setState(() {
              _dragging = false;
            });

            _startHideTimer();
          },
          colors: chewieController.materialProgressColors ??
              ChewieProgressColors(
                playedColor: Colors.blue,
                handleColor:  Colors.blue,
                bufferedColor: Colors.white,
                // backgroundColor: Colors.black
              )

                  // playedColor: Theme.of(context).accentColor,
                  // handleColor: Theme.of(context).accentColor,
                  // bufferedColor: Theme.of(context).backgroundColor,
                  // backgroundColor: Theme.of(context).disabledColor),
        ),
      ),
    );
  }
}


class VideoChangeQuality extends StatefulWidget{
  const VideoChangeQuality({Key key, this.url,this.duration,this.title,this.numberEpisode,this.islive,this.isallowFullScreen, this.quality}) : super(key : key);
  final String url;
  final Duration duration;
  final String title;
  final int numberEpisode;
  final bool islive;
  final bool isallowFullScreen;
  final List<dynamic> quality;


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StateVideoChangeQuality();
  }
}

class StateVideoChangeQuality extends State<VideoChangeQuality>{
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;
  @override
  void initState() {
    _videoPlayerController = VideoPlayerController.network(widget.url);
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController,
      autoPlay: true,
       aspectRatio: 16 / 9,
        isLive: widget.islive,
        durationSpecific: widget.duration,
        allowFullScreen: widget.isallowFullScreen,
        titleVideo: widget.title,
        // titleSerie: '',
        videoQuality: widget.quality,
        closeButton: Platform.isIOS ? true : false,
    );
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    
    // TODO: implement build
    return WillPopScope(
        onWillPop: () {
          Navigator.pop(context);
          return Future.value(false);
        },
        child: Scaffold(
      backgroundColor: Colors.black,
      body: Chewie(
        controller: _chewieController,
      ),
    ));
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _chewieController.dispose();
    _videoPlayerController.dispose();
  }
}
