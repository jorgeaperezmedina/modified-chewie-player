import 'package:chewie/src/chewie_progress_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:video_player/video_player.dart';

import '../chewie.dart';

class CupertinoVideoProgressBar extends StatefulWidget {
  CupertinoVideoProgressBar(
    this.controller, 
    // this.onDuration,
    this.chewieController,
    {
    ChewieProgressColors colors,
    this.onDragEnd,
    this.onDragStart,
    this.onDragUpdate,
    this.durationVideo
  }) : colors = colors ?? ChewieProgressColors();
  final ChewieController chewieController;
  final VideoPlayerController controller;
  final ChewieProgressColors colors;
  final Function() onDragStart;
  final Function() onDragEnd;
  final Function() onDragUpdate;
  final Duration durationVideo;
  // ChewieController onDuration;
  // final Duration onDuration;

  @override
  _VideoProgressBarState createState() {
    return _VideoProgressBarState();
  }
}

class _VideoProgressBarState extends State<CupertinoVideoProgressBar> {
  _VideoProgressBarState() {
    listener = () {
      setState(() {});
    };
  }
  
  // ChewieController chewieController;
  VoidCallback listener;
  bool _controllerWasPlaying = false;
  // ChewieController get chewieController => widget.chewieController;
  VideoPlayerController get controller => widget.controller;
  ChewieController get _chewieController => widget.chewieController;
  @override
  void initState() {
    super.initState();
    controller.addListener(listener);
  }

  @override
  void deactivate() {
    controller.removeListener(listener);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
                  // print(widget.durationVideo);
    // print(controller);
    void seekToRelativePosition(Offset globalPosition) {
      final box = context.findRenderObject() as RenderBox;
      final Offset tapPos = box.globalToLocal(globalPosition);
      final double relative = tapPos.dx / box.size.width;
      final Duration position =  widget.durationVideo * relative;
      // print( "position relativa  " + position.toString());
      // final Duration position = controller.value.duration * relative;
      // print(relative);
      // controller.durationChewie(widget.durationVideo);
      controller.seekTo(position);
      // controller.seekTo(position);
      // print(chewieController.seekTo(position));
      // print(controller.seekTo(position).hashCode);
    }

    return GestureDetector(
      child: Center(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.transparent,
          child:  CustomPaint(
            painter: _ProgressBarPainter(
              // widget.onDuration,
              controller.value,
              widget.colors,
              widget.durationVideo
              // controller.value.duration
              // chewieController.durationSpecific,
              // widget.chewieController,
            ),
          ),
        ),
      ),
      onHorizontalDragStart: (DragStartDetails details) {
        if (!controller.value.initialized) {
          return;
        }
        _controllerWasPlaying = controller.value.isPlaying;
        if (_controllerWasPlaying) {
          controller.pause();
        }

        if (widget.onDragStart != null) {
          widget.onDragStart();
        }
      },
      onHorizontalDragUpdate: (DragUpdateDetails details) {
        if (!controller.value.initialized) {
          return;
        }
        
        seekToRelativePosition(details.globalPosition);

        if (widget.onDragUpdate != null) {
          // print('controller initialied ' + controller.value.initialized.toString());
          widget.onDragUpdate();
        }
      },
      onHorizontalDragEnd: (DragEndDetails details) {
        if (_controllerWasPlaying) {
          controller.play();
        }

        if (widget.onDragEnd != null) {
          widget.onDragEnd();
        }
      },
      onTapDown: (TapDownDetails details) {
        if (!controller.value.initialized) {
          return;
        }
        // print(controller.value.duration);
        // if (controller.value.duration == Duration.zero) {
        //   print('error de duracion 0000000000000');
        //   return;
        // }
        // if(widget.durationVideo != Duration.zero || widget.durationVideo != null){
        //   seekToRelativePosition(details.globalPosition);
        // }
      
            seekToRelativePosition(details.globalPosition);
      },
    );
  }
}

class _ProgressBarPainter extends CustomPainter {
  _ProgressBarPainter(this.value, this.colors, this.duration);

  VideoPlayerValue value;
  ChewieProgressColors colors;
  Duration duration;
//  ChewieController chewieController;

  @override
  bool shouldRepaint(CustomPainter painter) {
    // print(duration);
    // print(duration.inMilliseconds);
    // print(chewieController.durationSpecific);
    return true;
  }

  @override
  void paint(Canvas canvas, Size size) {

    final barHeight = 5.0;
    final handleHeight = 6.0;
    final baseOffset = size.height / 2 - barHeight / 2.0;

    canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromPoints(
          Offset(0.0, baseOffset),
          Offset(size.width, 
          baseOffset + barHeight),
        ),
        Radius.circular(4.0),
      ),
      colors.backgroundPaint,
    );
    if (!value.initialized) {
      return;
    }
   
   final double playedPartPercent =
   value.position.inMilliseconds / duration.inMilliseconds;
        // value.position.inMilliseconds / value.duration.inMilliseconds;
    final double playedPart =
        playedPartPercent > 1 ? size.width : playedPartPercent * size.width;
  
// if (value.buffered == null || value.buffered.isEmpty  || value.duration == null || value.duration == Duration.zero ) {
//    return;
// } else{
   for (DurationRange range in value.buffered) {
     final double start = range.startFraction(duration) * size.width;
      // final double start = range.startFraction(value.duration) * size.width;
      final double end = range.endFraction(duration) * size.width;
      // final double end = range.endFraction(value.duration) * size.width;
      canvas.drawRRect(
        RRect.fromRectAndRadius(
          Rect.fromPoints(
            Offset(start, baseOffset),
            Offset(end, baseOffset + barHeight),
          ),
          Radius.circular(4.0),
        ),
        colors.bufferedPaint,
      );
    }
     canvas.drawRRect(
      RRect.fromRectAndRadius(
        Rect.fromPoints(
          Offset(0.0, baseOffset),
          Offset(playedPart, baseOffset + barHeight),
        ),
        Radius.circular(4.0),
      ),
      colors.playedPaint,
    );
    final shadowPath = Path()
      ..addOval(Rect.fromCircle(
          center: Offset(playedPart, baseOffset + barHeight / 2),
          radius: handleHeight));

    canvas.drawShadow(shadowPath, Colors.black, 0.2, false);
    canvas.drawCircle(
      Offset(playedPart, baseOffset + barHeight / 2),
      handleHeight,
      colors.handlePaint,
    );
// }

    // if (value.buffered == null || value.buffered.isEmpty  || value.duration == null || value.position > Duration.zero) {
    //   return;
    // }else{
    //   canvas.drawRRect(
    //   RRect.fromRectAndRadius(
    //     Rect.fromPoints(
    //       Offset(0.0, baseOffset),
    //       Offset(playedPart, baseOffset + barHeight),
    //     ),
    //     Radius.circular(4.0),
    //   ),
    //   colors.playedPaint,
    // );
    // final shadowPath = Path()
    //   ..addOval(Rect.fromCircle(
    //       center: Offset(playedPart, baseOffset + barHeight / 2),
    //       radius: handleHeight));

    // canvas.drawShadow(shadowPath, Colors.black, 0.2, false);
    // canvas.drawCircle(
    //   Offset(playedPart, baseOffset + barHeight / 2),
    //   handleHeight,
    //   colors.handlePaint,
    // );
    // }

    
  }
}
