import 'dart:async';
import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:chewie/src/chewie_player.dart';
import 'package:chewie/src/chewie_progress_colors.dart';
import 'package:chewie/src/cupertino_progress_bar.dart';
import 'package:chewie/src/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:open_iconic_flutter/open_iconic_flutter.dart';
import 'package:video_player/video_player.dart';

class CupertinoControls extends StatefulWidget {
  const CupertinoControls({
    @required this.backgroundColor,
    @required this.iconColor,
  });

  final Color backgroundColor;
  final Color iconColor;

  @override
  State<StatefulWidget> createState() {
    return _CupertinoControlsState();
  }
}
//_chewieController.videoPlayerController.durationChewie(widget.duration);

class _CupertinoControlsState extends State<CupertinoControls> {
  VideoPlayerValue _latestValue;
  double _latestVolume;
  bool _hideStuff = true;
  Timer _hideTimer;
  final marginSize = 5.0;
  Timer _expandCollapseTimer;
  Timer _initTimer;

  VideoPlayerController controller;
  ChewieController chewieController;

  @override
  Widget build(BuildContext context) {
    chewieController = ChewieController.of(context);
    // print(chewieController.videoPlayerController);
  //   print('el valor del player es buffering');
  // print(_latestValue.isBuffering);
  //  print('el valor del player tiene error?');
  // print(_latestValue.hasError);
  // print('¿isBufering?');
  //   print(controller.value.isBuffering);

    // if (_latestValue.hasError) {
    //   return chewieController.errorBuilder != null
    //       ? chewieController.errorBuilder(
    //           context,
    //           chewieController.videoPlayerController.value.errorDescription,
    //         )
    //       : Center(
    //         child: CupertinoActivityIndicator(),
    //       );
    //       // Center(
    //       //     child: Icon(
    //       //       OpenIconicIcons.ban,
    //       //       color: Colors.white,
    //       //       size: 42,
    //       //     ),
    //       //   );
    // }

    final backgroundColor = widget.backgroundColor;
    final iconColor = widget.iconColor;
    chewieController = ChewieController.of(context);
    controller = chewieController.videoPlayerController;
    final orientation = MediaQuery.of(context).orientation;
    final barHeight = orientation == Orientation.portrait ? 35.0 : 47.0;
    final buttonPadding = orientation == Orientation.portrait ? 16.0 : 24.0;

    return MouseRegion(
      onHover: (_) {
        _cancelAndRestartTimer();
      },
      child: Container(
        decoration: BoxDecoration(
          color: _hideStuff ? Colors.transparent : Colors.black.withOpacity(0.5),
        ),
        child: GestureDetector(
        onTap: () {
          _cancelAndRestartTimer();
        },
        child: AbsorbPointer(
          absorbing: _hideStuff,
          child: Column(
            children: <Widget>[

              _buildTopBar(backgroundColor, iconColor, barHeight, buttonPadding),
            changeIcons(backgroundColor, iconColor, barHeight),
              // _latestValue != null &&
              //             !_latestValue.isPlaying &&
              //             _latestValue.duration == null && chewieController.videoPlayerController.value.isPlaying == false||
              //         _latestValue.isBuffering || controller.value.hasError 
              //     ? 
              //     const Expanded(
              //         child: const Center(
              //           child: const CupertinoActivityIndicator(),
              //         ),
              //       )
              //     : 
              // _buildHitArea(),
              // titleSerie(backgroundColor),
              // titleVideo( backgroundColor),
             
              _buildBottomBar(backgroundColor, iconColor, barHeight),
            ],
          ),
        ),
      ),
      ),
    );
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _dispose() {
    controller.removeListener(_updateState);
    _hideTimer?.cancel();
    _expandCollapseTimer?.cancel();
    _initTimer?.cancel();
  }

  @override
  void didChangeDependencies() {
    final _oldController = chewieController;
    chewieController = ChewieController.of(context);
    controller = chewieController.videoPlayerController;

    if (_oldController != chewieController) {
      _dispose();
      _initialize();
    }

    super.didChangeDependencies();
  }

 Widget changeIcons(
  Color backgroundColor,
    Color iconColor,
    double barHeight){
    if (_latestValue != null &&
          !_latestValue.isPlaying &&
             _latestValue.duration == null && controller.value.isPlaying == false||
                 _latestValue.isBuffering == true  || controller.value.hasError 
                  ) {
       return const Expanded(
                      child: const Center(
                        child: const CupertinoActivityIndicator(),
                      ),
                    );
    } else {
      // centerButtons(backgroundColor, iconColor, barHeight);
       return _buildHitArea();
    }
}

  AnimatedOpacity centerButtons(
    Color backgroundColor,
    Color iconColor,
    double barHeight
  ){
    return AnimatedOpacity(
      duration: Duration(milliseconds:  300),
      opacity: _hideStuff ? 0.0 : 0.9,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5)
        ),
        margin: EdgeInsets.only(left: 5, right: 5, top: 5),
        // color: backgroundColor,
        alignment: Alignment.topLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildSkipBack(iconColor, barHeight),
            _buildPlayPause(controller, iconColor, barHeight),
            _buildSkipForward(iconColor, barHeight),
          ],
        ),
        // Text(' ' + 
        //   chewieController.titleSerie + ' ', 
          // style: TextStyle(
          //   backgroundColor: Colors.black26,
          //   color: Colors.white, 
          //   fontSize: 20)
            // ,),
      ),
    );
  }


  AnimatedOpacity titleSerie(
    Color backgroundColor
  ){
    return AnimatedOpacity(
      duration: Duration(milliseconds:  300),
      opacity: _hideStuff ? 0.0 : 0.9,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5)
        ),
        margin: EdgeInsets.only(left: 5, right: 5, top: 5),
        // color: backgroundColor,
        alignment: Alignment.topLeft,
        child: Text(''),
        // Text(' ' + 
        //   chewieController.titleSerie + ' ', 
          // style: TextStyle(
          //   backgroundColor: Colors.black26,
          //   color: Colors.white, 
          //   fontSize: 20)
            // ,),
      ),
    );
  }

  AnimatedOpacity titleVideo(
    Color backgroundColor
  ){
    return AnimatedOpacity(
      duration: Duration(milliseconds:  300),
      opacity: _hideStuff ? 0.0 : 0.9,
      child: Container(
        margin: EdgeInsets.only(left: 0, right: 0, top: 0),
        // color: Colors.black45,
        // color: backgroundColor,
        alignment: Alignment.center,
                child: chewieController.isLive ? Text('') : Text(chewieController.titleVideo, 

        // child: chewieController.isLive ? Text('') : Text(' ' + 'Ep.${chewieController.numberEpisode.toString()}'+ "  " +chewieController.titleVideo+' ', 
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          shadows: [
            Shadow(
              color: Colors.black,
              blurRadius: 10.0,
              offset: Offset(5.0, 5.0)
            ),
             Shadow(
              color: Colors.black,
              blurRadius: 10.0,
              offset: Offset(5.0, 5.0)
            )
          ],
          // backgroundColor: Colors.black45,
          color: Colors.white, 
        fontSize: 20,)
        ,),
      ),
    );
  }

  AnimatedOpacity _buildBottomBar(
    Color backgroundColor,
    Color iconColor,
    double barHeight,
  ) {
    // print(chewieController.videoPlayerController);
    // print(chewieController.durationSpecific);
    // print(chewieController.videoPlayerController);
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: Duration(milliseconds: 300),
      child: Container(
        color: Colors.transparent,
        alignment: Alignment.bottomCenter,
        margin: EdgeInsets.all(marginSize),
        child:  
        // _latestValue == null || chewieController.durationSpecific == null  || chewieController.durationSpecific == Duration.zero ? 
        // Text(''):
        // :
        ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: BackdropFilter(
            filter: ui.ImageFilter.blur(
              sigmaX: 10.0,
              sigmaY: 10.0,
            ),
            child: Container(
              height: barHeight,
              color: backgroundColor,
              child: chewieController.isLive
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        _buildPlayPause(controller, iconColor, barHeight),
                        _buildLive(iconColor),
                      ],
                    )
                  : Row(
                      children: <Widget>[
                        // _buildSkipBack(iconColor, barHeight),
                        _buildPlayPause(controller, iconColor, barHeight),
                        // _buildSkipForward(iconColor, barHeight),
                        _buildPosition(iconColor),
                        _buildProgressBar(),
                        _buildRemaining(iconColor)
                      ],
                    ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildLive(Color iconColor) {
    return Padding(
      padding: EdgeInsets.only(right: 12.0),
      child: Text(
        'En vivo',
        style: TextStyle(color: iconColor, 
        fontSize: 17.0,
        fontWeight: FontWeight.bold
        ),
      ),
    );
  }

  GestureDetector _buildExpandButton(
    Color backgroundColor,
    Color iconColor,
    double barHeight,
    double buttonPadding,
  ) {
    return GestureDetector(
      onTap: _onExpandCollapse,
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 10.0),
            child: Container(
              height: barHeight,
              padding: EdgeInsets.only(
                left: buttonPadding,
                right: buttonPadding,
              ),
              color: backgroundColor,
              child: Center(
                child: Icon(
                  chewieController.isFullScreen
                      ? OpenIconicIcons.fullscreenExit
                      : OpenIconicIcons.fullscreenEnter,
                  color: iconColor,
                  size: 20.0,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Expanded _buildHitArea() {
    return Expanded(
      child: GestureDetector(
        onTap: _latestValue != null && _latestValue.isPlaying
            ? _cancelAndRestartTimer
            : () {
                _hideTimer?.cancel();

                setState(() {
                  _hideStuff = false;
                });
              },
        child: Container(
          color: Colors.transparent,
        ),
      ),
    );
  }

  GestureDetector _buildMuteButton(
    VideoPlayerController controller,
    Color backgroundColor,
    Color iconColor,
    double barHeight,
    double buttonPadding,
  ) {
    return GestureDetector(
      onTap: () {
        _cancelAndRestartTimer();

        if (_latestValue.volume == 0) {
          controller.setVolume(_latestVolume ?? 0.5);
        } else {
          _latestVolume = controller.value.volume;
          controller.setVolume(0.0);
        }
      },
      child: AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: Duration(milliseconds: 300),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 10.0),
            child: Container(
              color: backgroundColor,
              child: Container(
                height: barHeight,
                padding: EdgeInsets.only(
                  left: buttonPadding,
                  right: buttonPadding,
                ),
                child: Icon(
                  (_latestValue != null && _latestValue.volume > 0)
                      ? Icons.volume_up
                      : Icons.volume_off,
                  color: iconColor,
                  size: 20.0,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  

  GestureDetector _buildPlayPause(
    VideoPlayerController controller,
    Color iconColor,
    double barHeight,
  ) {
    // print(chewieController.videoPlayerController.value.isPlaying);
    return GestureDetector(
      onTap: _playPause,
      // _latestValue == null || chewieController.durationSpecific == null || chewieController.durationSpecific == Duration.zero ? null : _playPause,
      child: _latestValue == null && chewieController.isLive == false || chewieController.durationSpecific == null && chewieController.isLive == false || chewieController.durationSpecific == Duration.zero && chewieController.isLive == false || _latestValue.position == Duration.zero && chewieController.isLive == false ? 
        Text('') :   
      Container(
        height: barHeight,
        color: Colors.transparent,
        padding: EdgeInsets.only(
          left: 12.0,
          right: 12.0,
        ),
        child: Icon(
          controller.value.isPlaying
              ? OpenIconicIcons.mediaPause
              : OpenIconicIcons.mediaPlay,
          color: iconColor,
          size: 20.0,
        ),
      ),
    );
  }

  Widget _buildPosition(Color iconColor) {
    final position =
        _latestValue != null ? _latestValue.position : Duration(seconds: 0);

    return Padding(
      padding: EdgeInsets.only(right: 12.0),
      child: Text(
        formatDuration(position),
        style: TextStyle(
          color: iconColor,
          fontSize: 15.0,
        ),
      ),
    );
  }

  Widget _buildRemaining(Color iconColor) {
    // print(_latestValue.size);
    // print("_latestValue "+ _latestValue.toString());
    // print("_latestValue.duration " + _latestValue.duration.toString());
    // print("position "+_latestValue.position.toString());
    // print("_latestValue.buffered " + _latestValue.buffered.toString());
    // if (value.buffered == null || value.buffered.isEmpty  || value.duration == null || value.duration == Duration.zero ) {
      // print(chewieController.durationSpecific);

    final position = _latestValue != null && chewieController.durationSpecific != null  
    // || _latestValue.duration != Duration.zero 
    || chewieController.durationSpecific != Duration.zero
    || _latestValue.position != Duration.zero ?
      chewieController.durationSpecific - _latestValue.position
    //  _latestValue.duration - _latestValue.position
        : Duration.zero;

    return Padding(
      padding: EdgeInsets.only(right: 12.0),
      child: _latestValue == null || chewieController.durationSpecific == null || chewieController.durationSpecific == Duration.zero
       ? Text(
        '0.0',
        style: TextStyle(color: iconColor, fontSize: 15.0),
      ) 
       : Text(
        '-${formatDuration(position)}',
        style: TextStyle(color: iconColor, fontSize: 15.0),
      ),
    );
  }

  GestureDetector _buildSkipBack(Color iconColor, double barHeight) {
    return GestureDetector(
      onTap: _skipBack,
      child: _latestValue == null || chewieController.durationSpecific == null  || chewieController.durationSpecific == Duration.zero || _latestValue.position == Duration.zero? 
        Text('') :   
      Container(
        height: barHeight,
        color: Colors.transparent,
        margin: EdgeInsets.only(left: 10.0),
        padding: EdgeInsets.only(
          left: 6.0,
          right: 6.0,
        ),
        child: Transform(
          alignment: Alignment.center,
          transform: Matrix4.skewY(0.0)
            ..rotateX(math.pi)
            ..rotateZ(math.pi),
          child: Icon(
            OpenIconicIcons.reload,
            color: iconColor,
            size: 20.0,
          ),
        ),
      ),
    );
  }

  GestureDetector _buildSkipForward(Color iconColor, double barHeight) {
    return GestureDetector(
      onTap:  _skipForward,
      child: _latestValue == null || chewieController.durationSpecific == null  || chewieController.durationSpecific == Duration.zero || _latestValue.position == Duration.zero? 
        Text('') :   
      Container(
        height: barHeight,
        color: Colors.transparent,
        padding: EdgeInsets.only(
          left: 6.0,
          right: 8.0,
        ),
        margin: EdgeInsets.only(
          right: 8.0,
        ),
        child: Icon(
          OpenIconicIcons.reload,
          color: iconColor,
          size: 20.0,
        ),
      ),
    );
  }

  Widget _buildTopBar(
    Color backgroundColor,
    Color iconColor,
    double barHeight,
    double buttonPadding,
  ) {
    return Container(
      height: barHeight,
      margin: EdgeInsets.only(
        top: marginSize,
        right: marginSize,
        left: marginSize,
      ),
      child: Row(
        children: <Widget>[
          chewieController.allowFullScreen
              ? _buildExpandButton(
                  backgroundColor, iconColor, barHeight, buttonPadding)
              : Container(),
              chewieController.closeButton && !chewieController.allowFullScreen ?
              _buildCloseButton(backgroundColor, iconColor, barHeight, buttonPadding, context):
              Container(),
          Expanded(child: Container(
            child: !chewieController.allowFullScreen || chewieController.allowFullScreen ? titleVideo(backgroundColor) : Container(),
          )),
           
           
          chewieController.allowMuting
              ? _buildMuteButton(controller, backgroundColor, iconColor,
                  barHeight, buttonPadding)
              : Container(),
        ],
      ),
    );
  }


  void _cancelAndRestartTimer() {
    _hideTimer?.cancel();

    setState(() {
      _hideStuff = false;

      _startHideTimer();
    });
  }

  Future<Null> _initialize() async {
    controller.addListener(_updateState);

    _updateState();

    if ((controller.value != null && controller.value.isPlaying) ||
        chewieController.autoPlay) {
      _startHideTimer();
    }

    if (chewieController.showControlsOnInitialize) {
      _initTimer = Timer(Duration(milliseconds: 200), () {
        setState(() {
          _hideStuff = false;
        });
      });
    }
  }

  void _onExpandCollapse() {
    setState(() {
      _hideStuff = true;

      chewieController.toggleFullScreen();
      _expandCollapseTimer = Timer(Duration(milliseconds: 300), () {
        setState(() {
          _cancelAndRestartTimer();
        });
      });
    });
  }

  Widget _buildProgressBar() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(right: 12.0),
        child: 
        _latestValue == null || chewieController.durationSpecific == null  || chewieController.durationSpecific == Duration.zero || _latestValue.position == Duration.zero? 
        Text('') :  
        CupertinoVideoProgressBar(
          
          controller,
          chewieController,
          // chewieController,
          // onDuration: chewieController.durationSpecific,
          //  chewieController,
          durationVideo: chewieController.durationSpecific,

          onDragStart: () {
            _hideTimer?.cancel();
          },
          onDragEnd: () {
            _startHideTimer();
          },
          colors: chewieController.cupertinoProgressColors ??
              ChewieProgressColors(
                playedColor: Color.fromARGB(
                  120,
                  255,
                  255,
                  255,
                ),
                handleColor: Color.fromARGB(
                  255,
                  255,
                  255,
                  255,
                ),
                bufferedColor: Color.fromARGB(
                  60,
                  255,
                  255,
                  255,
                ),
                backgroundColor: Color.fromARGB(
                  20,
                  255,
                  255,
                  255,
                ),
              ),
        ),
      ),
    );
  }
  GestureDetector _buildCloseButton(
    Color backgroundColor,
    Color iconColor,
    double barHeight,
    double buttonPadding,
    BuildContext context,
  ){
    
  return GestureDetector(
    onTap: (){
      controller.pause();
      Navigator.pop(context);

    },
    child: AnimatedOpacity(
      opacity:  _hideStuff ? 0.0 : 1.0,
      duration: Duration(milliseconds: 300),
      child: ClipRect(
        child: BackdropFilter(
          filter: ui.ImageFilter.blur(sigmaX: 10.0),
          child: Container(
            height: barHeight,
            padding: EdgeInsets.only(
              left: buttonPadding,
              right: buttonPadding,
              ),
              decoration: BoxDecoration(
                color: backgroundColor,
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              child: Center(
                child: Icon(
                  OpenIconicIcons.x,
                  color: iconColor,
                  size: 15,
                ),
              ),
          ),
        ),
      ),
    ),
  );
  }

  void _playPause() {
      // Duration durations = chewieController.durationSpecific == Duration(seconds: 0) ? _latestValue.duration : chewieController.durationSpecific;
    bool isFinished = _latestValue.position >=  chewieController.durationSpecific ;
    
    // bool isFinished = _latestValue.position >= _latestValue.duration;
  

    setState(() {
      if (controller.value.isPlaying) {
        _hideStuff = false;
        _hideTimer?.cancel();
        controller.pause();
      } else {
        _cancelAndRestartTimer();

        if (!controller.value.initialized) {
          controller.initialize().then((_) {
            controller.play();
          });
        } else {
          if (isFinished) {
            chewieController.seekTo(Duration.zero);
            controller.seekTo(Duration.zero);
          }
          controller.play();
        }
      }
    });
  }

  void _skipBack() {
    if (chewieController.durationSpecific != null || chewieController.durationSpecific != Duration.zero) {
      print('llegue a dentro del if');
       _cancelAndRestartTimer();
    final beginning = Duration(seconds: 0).inMilliseconds;
    final skip = (_latestValue.position - Duration(seconds: 10)).inMilliseconds;
    controller.seekTo(Duration(milliseconds: math.max(skip, beginning)));
    } else {
      print('error');
    }
  }

  void _skipForward() {

    try {
      _cancelAndRestartTimer();
    final end = chewieController.durationSpecific.inMilliseconds;
    // final end = _latestValue.duration.inMilliseconds;
    final skip = (_latestValue.position + Duration(seconds: 10)).inMilliseconds;
    // var time = math.min(skip, end);
    // print(chewieController.videoPlayerController.value.duration);
    controller.seekTo(Duration(milliseconds: math.min(skip, end) ));
    } catch (e) {
      print(e);
    }
  }

  void _startHideTimer() {
    _hideTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  

  void _updateState() {
    // _changeDuration();
    setState(() {
      _latestValue = controller.value;

    });
  }
}
